$(document).ready(function(){
	
	initTab();

	initMoreBtn();

	showArticleList(0);
})




function initTab(){
	$('.tabWrap .tabItem').on('click',function(){
		
		showArticleList($(this).index());
	});

	$('.tagBtnWrap span').on('click',function(){
		
		showArticleList($(this).index());
	});
}

function showArticleList(i){

	$('.tabWrap .tabItem').eq(i).addClass('active').siblings().removeClass('active');
	$('.articleListWrap .articleList').eq(i).fadeIn().siblings().hide();
	$('.tagBtnWrap span').eq(i).addClass('active').siblings().removeClass('active');
}

function initMoreBtn(){
	$('.articleList .moreBtn').on('click',function(){
		$(this).hide().siblings('.hide').removeClass('hide');
	});
}
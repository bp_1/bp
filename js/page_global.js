$(document).ready(function() {
    initCommonItem();
    checkWindowScroll();
    initHeader();
    initLimitTxt();
    $('.coverPic').css("background-size", "cover");
    $('.containPic').css("background-size", "contain");
})

function initLimitTxt() {


    $('.limitTxt').each(function() {
        var t = $(this);
        var $limit = parseInt(t.attr('data-limit'), 10);
        var $showBtn = (t.attr('data-showBtn') == 'true') ? true : false;


        if ($limit != undefined && $limit != '') {


            var $str = t.text(); // Getting the text
            $str = $str.replace(/ {2,}/g, ' ');
            $str = $str.replace(/\n\s*\n/g, '\n');

            if ($str.length > $limit) {
                if (t.hasClass('showBtnType2')) {
                    var $strtemp = '<span class="hide">' + $str.substr($limit, $str.length) + '</span><a href="#" class="seeMoreContentBtn greenTxt f13">+ 展開<i class="i_greenDArr"></i></a>';
                    

                } else {
                    var $strtemp = '<span class="hide">' + $str.substr($limit, $str.length) + '</span><a href="#" class="seeMoreContentBtn grayTxt f12">+展開全部</a>';

                }
                $str = $str.substr(0, $limit) + '<i class="dot"> ...</i>';

                if ($showBtn) {
                    $str += $strtemp;
                }

                t.html($str);

            }

        }


    });

    $('.seeMoreContentBtn').on('click', function(e) {
        e.preventDefault();
        $(this).addClass('hide').siblings('.hide').removeClass('hide').siblings('.dot').addClass('hide');
    });
}

function checkWindowScroll() {
    var deadLine = 115;

    $(window).scroll(function(event) {
        var scroll = $(window).scrollTop();
        if (scroll > deadLine) {
            if (!$('#pageNav').hasClass('fixMenu')) {
                $('#pageNav').addClass('fixMenu');
            }
        } else {
            if ($('#pageNav').hasClass('fixMenu')) {
                $('#pageNav').removeClass('fixMenu');
            }
        }
    });
}

// load 共用內容
function initCommonItem() {
    $('#headerWrap').load('header.html');
    $('#mainRightBar').load('mainRightBar.html');
    $('#footer').load('footer.html');
    $('#popWrap').load('popWrap.html');
    $('#memberBar').load('memberBar.html');
    $('#parentOnlineBookStore').empty().load('parentOnlineBookStore.html');

}

function initPop() {
    initDropDown();
    $('.popPage .clozBtn').on('click', function() {
        $.colorbox.close();
    });
}

function jsSelectIsExitItem(objSelect, objItemValue) {
    var isExit = false;
    for (var i = 0; i < objSelect.options.length; i++) {
        if (objSelect.options[i].value == objItemValue) {
            isExit = true;
            break;
        }
    }
    return isExit;
}

function jsUpdateItemToSelect(objSelect, objItemText, objItemValue) {
    //判斷是否存在
    if (jsSelectIsExitItem(objSelect, objItemValue)) {
        for (var i = 0; i < objSelect.options.length; i++) {
            if (objSelect.options[i].value == objItemValue) {
                objSelect.options[i].text = objItemText;
                break;
            }
        }
        alert("成功修改");
    } else {
        alert("該select中 不存在該項");
    }
}

function initDropDown() {
    var yearBox = $('.year-select');
    var monthBox = $('.month-select');
    var year = new Date().getFullYear();

    yearBox.empty();
    monthBox.empty();


    yearBox.each(function() {
        var t = $(this);
        var val = t.attr('data-val');
        for (var i = 1900; i < year + 1; i++) {
            t.append('<option value="' + i + '">' + i + '</option>');
        }
        t.prepend('<option value="-1" selected>出生年</option>');



        if (val != undefined && val != '') {
            t.val(val);
        }



    });

    monthBox.each(function() {
        var t = $(this);
        var val = t.attr('data-val');
        for (var i = 1; i < 13; i++) {
            t.append('<option value="' + i + '">' + i + '</option>');
        }
        t.prepend('<option value="-1" selected>月</option>');
        if (val != undefined && val != '') {
            t.val(val);
        }

    });
}

// init 右邊網友推薦內容
function initMainRightBar() {

    $('#r_recommendRankBox').find('.tabItem').on('click', function() {
        if (!$(this).hasClass('active')) {
            var t = $(this);
            t.addClass('active').siblings().removeClass('active');
            t.closest('.rankContainer').find('.rRB_articleWrap').find('.rBarList').eq(t.index()).removeClass('hide').siblings().addClass('hide');
        }
    });

    $('#r_hotQABox').find('.tabItem').on('click', function() {
        if (!$(this).hasClass('active')) {
            var t = $(this);
            t.addClass('active').siblings().removeClass('active');
            t.closest('.rankContainer').find('.rRB_articleWrap').find('.rBarList').eq(t.index()).removeClass('hide').siblings().addClass('hide');
        }
    });
}

function initHeader() {
    var offset;
    $(window).scroll(function() {
        offset = $(window).scrollTop();
        if (offset > 113) {
            if (!$('#pageNav').hasClass('fixNav'))
                $('#pageNav').addClass('fixNav');
        } else {
            if ($('#pageNav').hasClass('fixNav'))
                $('#pageNav').removeClass('fixNav');
        }

    });
}
